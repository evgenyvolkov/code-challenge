package me.tango.code.challenge.controller;

import lombok.RequiredArgsConstructor;
import me.tango.code.challenge.dto.TransactionDto;
import me.tango.code.challenge.model.Transaction;
import me.tango.code.challenge.service.TransactionService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import static org.springframework.http.HttpStatus.CREATED;

@RestController
@RequestMapping("/transactions")
@RequiredArgsConstructor
public class TransactionController {
	
	private final TransactionService transactionService;
	
	
	@ResponseStatus(CREATED)
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public void createTransaction(@RequestBody @Valid @NotNull final TransactionDto transactionDto) {
		
		Transaction transaction = Transaction.builder()
				.amount(transactionDto.getAmount())
				.timestamp(transactionDto.getTimestamp())
				.build();
		
		transactionService.checkTransactionValidity(transaction);
		transactionService.saveTransaction(transaction);
	}

}
