package me.tango.code.challenge.controller;

import lombok.RequiredArgsConstructor;
import me.tango.code.challenge.dto.StatisticsDto;
import me.tango.code.challenge.service.StatisticsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("/statistics")
@RequiredArgsConstructor
public class StatisticsController {
	
	private final StatisticsService statisticsService;
	
	
	@ResponseStatus(OK)
	@GetMapping
	public StatisticsDto getStatistics() {
		
		return statisticsService.getStatistics().toDto();
	}

}
