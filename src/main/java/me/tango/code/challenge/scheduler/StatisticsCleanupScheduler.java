package me.tango.code.challenge.scheduler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.tango.code.challenge.service.StatisticsService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Component
@RequiredArgsConstructor
@Slf4j
public class StatisticsCleanupScheduler {
  
    @Value("${statistics.cleanup-activation-time.millis}")
    private Long statisticsCleanupActivationTimeInMillis;
    
    private final StatisticsService statisticsService;
    
    
    @Scheduled(
            fixedDelayString = "${schedulers.statistics-cleanup-scheduler.delay}",
            initialDelayString = "${schedulers.statistics-cleanup-scheduler.delay}"
    )
    public void cleanupStatistics() {
        
        Instant currentTime      = Instant.now();
        long    currentTimeStamp = Instant.now().toEpochMilli();
        long    fromTimeStamp    = currentTimeStamp - statisticsCleanupActivationTimeInMillis;
        long    toTimeStamp      = currentTimeStamp - 61000L;
        
        if (statisticsCleanupActivationTimeInMillis <= 0 || fromTimeStamp <= 0) {
            throw new IllegalArgumentException("Check statistics.cleanup-activation-time.millis param in .yml is correct and restart app");
        }
        
        log.debug("Statistics cleanup started at: {}, human view: {}",
                currentTimeStamp, LocalDateTime.ofInstant(currentTime, ZoneId.systemDefault()));
        
        statisticsService.statisticsCleanUp(fromTimeStamp, toTimeStamp);
        
        currentTime = Instant.now();
        log.debug("Statistics cleanup finished at: {}, human view: {}",
                currentTime.toEpochMilli(), LocalDateTime.ofInstant(currentTime, ZoneId.systemDefault()));
    }
    
}