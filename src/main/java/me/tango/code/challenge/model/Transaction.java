package me.tango.code.challenge.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Transaction {
    
    private final double amount;
    private final long   timestamp;
    
}
