package me.tango.code.challenge.model;

import lombok.Builder;
import lombok.Data;
import me.tango.code.challenge.dto.StatisticsDto;

import java.math.BigDecimal;

@Data
@Builder
public class Statistics {
    
    private final double sum;
    private final double avg;
    private final double max;
    private final double min;
    private final long   count;
    
    
    public static Statistics empty() {
        
        return Statistics.builder()
                .sum(0)
                .avg(0)
                .max(0)
                .min(0)
                .count(0)
                .build();
    }
    
    public static Statistics fromTransaction(Transaction transaction) {
        
        double amount = transaction.getAmount();
        
        return Statistics.builder()
                .sum(amount)
                .avg(amount)
                .max(amount)
                .min(amount)
                .count(1)
                .build();
    }
    
    public Statistics withTransaction(Transaction transaction) {
        
        double amount = transaction.getAmount();
        
        return Statistics.builder()
                .sum(sum + amount)
                .avg((sum + amount) / (count + 1))
                .max(Math.max(amount, count == 0 ? Double.MIN_VALUE : max))
                .min(Math.min(amount, count == 0 ? Double.MAX_VALUE : min))
                .count(count + 1)
                .build();
    }
    
    public StatisticsDto toDto() {
        
        return StatisticsDto.builder()
                .sum(BigDecimal.valueOf(sum))
                .avg(BigDecimal.valueOf(avg))
                .max(BigDecimal.valueOf(max))
                .min(BigDecimal.valueOf(min))
                .count(count)
                .build();
    }
    
}
