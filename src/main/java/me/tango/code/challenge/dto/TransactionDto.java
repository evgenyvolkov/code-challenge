package me.tango.code.challenge.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class TransactionDto {
    
    @NotNull
    @Min(0)
    private final double amount;
    
    @NotNull
    @Min(0)
    private final long   timestamp;
    
}
