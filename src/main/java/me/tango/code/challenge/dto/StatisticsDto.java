package me.tango.code.challenge.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class StatisticsDto {
    
    @JsonSerialize(using = BigDecimalSerializer.class)
    private final BigDecimal sum;
    
    @JsonSerialize(using = BigDecimalSerializer.class)
    private final BigDecimal avg;
    
    @JsonSerialize(using = BigDecimalSerializer.class)
    private final BigDecimal max;
    
    @JsonSerialize(using = BigDecimalSerializer.class)
    private final BigDecimal min;
    
    private final long       count;
    
}
