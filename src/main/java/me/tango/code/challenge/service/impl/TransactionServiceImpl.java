package me.tango.code.challenge.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.tango.code.challenge.exception.InvalidTimestampException;
import me.tango.code.challenge.model.Transaction;
import me.tango.code.challenge.service.StatisticsService;
import me.tango.code.challenge.service.TransactionService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
@RequiredArgsConstructor
@Slf4j
public class TransactionServiceImpl implements TransactionService {
    
    @Value("${transaction.expiration-time.millis}")
    private Long transactionExpirationTimeInMillis;

    private final StatisticsService statisticsService;
    
    
    @Override
    public void checkTransactionValidity(Transaction transaction) {
        
        long transactionTimestamp = transaction.getTimestamp();
        long currentTimestamp     = Instant.now().toEpochMilli();
        long diff                 = currentTimestamp - transactionTimestamp;
    
        if (diff > transactionExpirationTimeInMillis) {
            log.debug("Transaction is invalid [expired] and will be ignored, amount: {}, timestamp: {}",
                    transaction.getAmount(), transaction.getTimestamp());
            throw new InvalidTimestampException("Transaction is expired (timestamp is too old)");
        }
        if (diff <= 0) {
            log.debug("Transaction is invalid [from the future] and will be ignored, amount: {}, timestamp: {}",
                    transaction.getAmount(), transaction.getTimestamp());
            throw new InvalidTimestampException("Transaction is out of future window (timestamp is too young, not sync client)");
        }
    }
    
    @Override
    public void saveTransaction(Transaction transaction) {
        
        statisticsService.updateStatistics(transaction);
    
        log.debug("Transaction with amount: {} and timestamp: {} was saved", transaction.getAmount(), transaction.getTimestamp());
    }
    
}
