package me.tango.code.challenge.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.tango.code.challenge.model.Statistics;
import me.tango.code.challenge.model.Transaction;
import me.tango.code.challenge.service.StatisticsService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Map;
import java.util.stream.LongStream;

@Service
@RequiredArgsConstructor
@Slf4j
public class StatisticsServiceImpl implements StatisticsService {
    
    @Value("${transaction.expiration-time.millis}")
    private Long transactionExpirationTimeInMillis;
    
    private final Map<Long, Statistics> statisticsStorage;
    
    
    @Override
    public void updateStatistics(Transaction transaction) {
        
        long transactionTimestamp = transaction.getTimestamp();
        long transactionExpirationTimestamp = transaction.getTimestamp() + transactionExpirationTimeInMillis;
        
        for (long timeStamp = transactionTimestamp; timeStamp <= transactionExpirationTimestamp; timeStamp ++) {
            statisticsStorage.compute(timeStamp,
                    (key, value) -> (value == null) ? Statistics.fromTransaction(transaction) : value.withTransaction(transaction));
        }
    }
    
    @Override
    public Statistics getStatistics() {
        
        Long currentTimeStamp = Instant.now().toEpochMilli();
        Statistics statistics = statisticsStorage.get(currentTimeStamp);
        
        return statistics != null ? statistics : Statistics.empty();
    }
    
    @Override
    public void statisticsCleanUp(long fromTimeStamp, long toTimeStamp) {
        
        if (fromTimeStamp < toTimeStamp) {
            LongStream.range(fromTimeStamp, toTimeStamp).forEach(it -> statisticsStorage.computeIfPresent(it,
                    (key, value) -> null));
        }
    }

}
