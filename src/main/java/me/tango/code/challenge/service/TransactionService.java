package me.tango.code.challenge.service;

import me.tango.code.challenge.model.Transaction;

public interface TransactionService {
    
    void checkTransactionValidity(Transaction transaction);
    
    void saveTransaction(Transaction transaction);
    
}
