package me.tango.code.challenge.service;

import me.tango.code.challenge.model.Statistics;
import me.tango.code.challenge.model.Transaction;

public interface StatisticsService {
    
    void updateStatistics(Transaction transaction);
    
    Statistics getStatistics();
    
    void statisticsCleanUp(long fromTimeStamp, long toTimeStamp);
    
}
