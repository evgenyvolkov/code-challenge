package me.tango.code.challenge.config;

import me.tango.code.challenge.model.Statistics;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;

@Configuration
@EnableScheduling
public class AppConfig {
    
    @Bean
    public Map<Long, Statistics> statisticsStorage() {
        return new ConcurrentHashMap<>();
    }
    
    @Bean
    public TaskScheduler taskExecutor() {
        return new ConcurrentTaskScheduler(Executors.newScheduledThreadPool(3));
    }
    
    @Bean
    public CommonsRequestLoggingFilter requestLoggingFilter() {
        
        CommonsRequestLoggingFilter filter = new CommonsRequestLoggingFilter();
        
        filter.setIncludeClientInfo(true);
        filter.setIncludeQueryString(true);
        filter.setIncludePayload(true);
        filter.setMaxPayloadLength(10000);
        filter.setIncludeHeaders(false);
        filter.setAfterMessagePrefix("REQUEST DATA : ");
        
        return filter;
    }
    
}
