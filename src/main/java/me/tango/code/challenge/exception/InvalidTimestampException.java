package me.tango.code.challenge.exception;

public class InvalidTimestampException extends IllegalArgumentException {
    
    public InvalidTimestampException(String message) {
        super(message);
    }
    
}
