package me.tango.code.challenge.unit;

import me.tango.code.challenge.controller.StatisticsController;
import me.tango.code.challenge.dto.StatisticsDto;
import me.tango.code.challenge.model.Statistics;
import me.tango.code.challenge.model.Transaction;
import me.tango.code.challenge.service.StatisticsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import static java.math.RoundingMode.HALF_UP;
import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(StatisticsController.class)
public class StatisticsControllerTest {
    
    @MockBean
    private StatisticsService statisticsService;
    
    @Resource
    private MockMvc mvc;
    
   
    @Test
    public void givenTransactions_whenGetStatistics_thenReturnStatistics() throws Exception {
        
        List<Transaction> transactionList = LongStream.range(0, 10)
                .mapToObj(x -> Transaction.builder()
                        .amount((double) x)
                        .timestamp(Instant.now().minusMillis(x).toEpochMilli())
                        .build()
                )
                .collect(Collectors.toList());
        
        Statistics statistics = Statistics.empty();
        for (Transaction item : transactionList) {
            statistics = statistics.withTransaction(item);
        }
        StatisticsDto statisticsDto = statistics.toDto();
        given(statisticsService.getStatistics()).willReturn(statistics);
        
        mvc.perform(MockMvcRequestBuilders.get("/statistics").contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("sum", is(statisticsDto.getSum().setScale(2, HALF_UP)), BigDecimal.class))
                .andExpect(jsonPath("avg", is(statisticsDto.getAvg().setScale(2, HALF_UP)), BigDecimal.class))
                .andExpect(jsonPath("max", is(statisticsDto.getMax().setScale(2, HALF_UP)), BigDecimal.class))
                .andExpect(jsonPath("min", is(statisticsDto.getMin().setScale(2, HALF_UP)), BigDecimal.class))
                .andExpect(jsonPath("count", is(statisticsDto.getCount()), long.class));
    }
    
}
