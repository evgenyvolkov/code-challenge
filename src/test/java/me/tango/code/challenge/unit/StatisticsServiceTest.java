package me.tango.code.challenge.unit;

import me.tango.code.challenge.model.Statistics;
import me.tango.code.challenge.model.Transaction;
import me.tango.code.challenge.service.StatisticsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringRunner.class)
public class StatisticsServiceTest {
    
    @MockBean
    private Map<Long, Statistics> statisticsStorage;
    
    @Resource
    private StatisticsService statisticsService;
    
    
    @Test
    public void givenNoTransactions_whenGetStatistics_thenReturnEmptyStatistics() {
        
        when(statisticsStorage.get(any(Long.class))).thenReturn(Statistics.empty());
        
        Statistics result = statisticsService.getStatistics();
        assertThat(result.getSum()).isEqualTo(0.0);
        assertThat(result.getAvg()).isEqualTo(0.0);
        assertThat(result.getMax()).isEqualTo(0.0);
        assertThat(result.getMin()).isEqualTo(0.0);
        assertThat(result.getCount()).isEqualTo(0);
    }
    
    @Test
    public void givenTransactions_whenGetStatistics_thenReturnStatistics() {
        
        List<Transaction> transactionList = LongStream.range(0, 10)
                .mapToObj(x -> Transaction.builder()
                        .amount((double) x)
                        .timestamp(Instant.now().minusMillis(x).toEpochMilli())
                        .build()
                )
                .collect(Collectors.toList());
        
        Statistics statistics = Statistics.empty();
        for (Transaction item : transactionList) {
            statistics = statistics.withTransaction(item);
        }
        
        when(statisticsStorage.get(any(Long.class))).thenReturn(statistics);
        
        Statistics result = statisticsService.getStatistics();
        assertThat(result.getSum()).isEqualTo(statistics.getSum());
        assertThat(result.getAvg()).isEqualTo(statistics.getAvg());
        assertThat(result.getMax()).isEqualTo(statistics.getMax());
        assertThat(result.getMin()).isEqualTo(statistics.getMin());
        assertThat(result.getCount()).isEqualTo(statistics.getCount());
    }
    
}
