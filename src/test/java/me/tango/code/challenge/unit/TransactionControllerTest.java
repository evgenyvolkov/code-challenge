package me.tango.code.challenge.unit;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.tango.code.challenge.controller.TransactionController;
import me.tango.code.challenge.model.Transaction;
import me.tango.code.challenge.service.TransactionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.annotation.Resource;
import java.time.Instant;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TransactionController.class)
public class TransactionControllerTest {
    
    @MockBean
    private TransactionService transactionService;
    
    @Resource
    private MockMvc mvc;
    
    @Autowired
    private ObjectMapper objectMapper;
    
    
    @Test
    public void givenValidTransaction_whenCreate_thenReturnCreated() throws Exception {
        
        Transaction transaction = Transaction.builder()
                .amount(100.0)
                .timestamp(Instant.now().toEpochMilli())
                .build();
    
        doNothing().when(transactionService).checkTransactionValidity(any(Transaction.class));
        doNothing().when(transactionService).saveTransaction(any(Transaction.class));
    
        mvc.perform(MockMvcRequestBuilders.post("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(transaction)))
                .andExpect(status().isCreated());
    }
    
}
