package me.tango.code.challenge.unit;

import me.tango.code.challenge.model.Transaction;
import me.tango.code.challenge.service.StatisticsService;
import me.tango.code.challenge.service.TransactionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.time.Instant;

import static org.mockito.Mockito.doNothing;

@SpringBootTest
@RunWith(SpringRunner.class)
public class TransactionServiceTest {
    
    @MockBean
    private StatisticsService statisticsService;
    
    @Resource
    private TransactionService transactionService;
    
    
    @Test
    public void givenTransaction_whenUpdateStatistics_thenDoNothing() {
  
        Transaction transaction = Transaction.builder()
                .amount(100.0)
                .timestamp(Instant.now().toEpochMilli())
                .build();
        
        doNothing().when(statisticsService).updateStatistics(transaction);
        transactionService.saveTransaction(transaction);
    }
    
}
