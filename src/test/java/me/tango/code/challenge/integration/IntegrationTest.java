package me.tango.code.challenge.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.tango.code.challenge.Application;
import me.tango.code.challenge.dto.StatisticsDto;
import me.tango.code.challenge.model.Statistics;
import me.tango.code.challenge.model.Transaction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import static java.math.RoundingMode.HALF_UP;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.MOCK, classes = Application.class)
@AutoConfigureMockMvc
public class IntegrationTest {
    
    @Resource
    private MockMvc mvc;
    
    
    @Test
    public void createdTransactions_givenTransactionsLast60Seconds_whenGetStatistics_thenReturnStatistics() throws Exception {
        
        List<Transaction> transactionList = LongStream.range(0, 10)
                .mapToObj(x -> Transaction.builder()
                        .amount((double) x)
                        .timestamp(Instant.now().minusMillis(x).toEpochMilli())
                        .build()
                )
                .collect(Collectors.toList());
        assertThat(transactionList.size()).isEqualTo(10);
    
        for (Transaction item : transactionList) {
            createTransaction(item);
        }
        
        Statistics statistics = Statistics.empty();
        for (Transaction item : transactionList) {
            statistics = statistics.withTransaction(item);
        }
        StatisticsDto statisticsDto = statistics.toDto();
        
        mvc.perform(MockMvcRequestBuilders.get("/statistics").contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("sum", is(statisticsDto.getSum().setScale(2, HALF_UP)), BigDecimal.class))
                .andExpect(jsonPath("avg", is(statisticsDto.getAvg().setScale(2, HALF_UP)), BigDecimal.class))
                .andExpect(jsonPath("max", is(statisticsDto.getMax().setScale(2, HALF_UP)), BigDecimal.class))
                .andExpect(jsonPath("min", is(statisticsDto.getMin().setScale(2, HALF_UP)), BigDecimal.class))
                .andExpect(jsonPath("count", is(statisticsDto.getCount()), long.class))
                .andDo(print());
    }
    
    private void createTransaction(Transaction tx) throws Exception {
        
        ObjectMapper mapper = new ObjectMapper();
        mvc.perform(MockMvcRequestBuilders.post("/transactions").contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(tx))).andExpect(status().isCreated()).andDo(print());
    }
    
}
