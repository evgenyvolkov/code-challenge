# Tango.me code challenge

## Short task info

We would like to have a restful API for our statistics. The main use case for our API is to
calculate realtime statistic from the last 60 seconds. There will be two APIs, one of them is
called every time a transaction is made. It is also the sole input of this rest API. The other one
returns the statistic based of the transactions of the last 60 seconds.

## Specs

#### POST /transactions

Every Time a new transaction happened, this endpoint will be called.
Body: { "amount": 12.3, "timestamp": 1478192204000 }

Where:

● amount - is a double specifying the amount

● timestamp - is a long specifying unix timeformat in milliseconds


Returns: Empty body with either 201 or 204.

● 201 - in case of success

● 204 - if transaction is older than 60 seconds


#### GET /statistics

This is the main endpoint of this task, this endpoint have to execute in constant time and
memory (O(1)). It returns the statistic based on the transactions which happened in the last 60
seconds.

Returns: { "sum": 1000, "avg": 100, "max": 200, "min": 50, "count": 10 }

Where:

● sum is a double specifying the total sum of transaction value in the last 60 seconds

● avg is a double specifying the average amount of transaction value in the last 60 seconds

● max is a double specifying single highest transaction value in the last 60 seconds

● min is a double specifying single lowest transaction value in the last 60 seconds

● count is a long specifying the total number of transactions happened in the last 60 seconds


## Requirements

For the rest api, the biggest and maybe hardest requirement is to make the GET /statistics
execute in constant time and space.

Other requirements, which are obvious, but also listed here explicitly:

● The API have to be threadsafe with concurrent requests

● The API have to function properly, with proper result

● The project should be buildable, and tests should also complete successfully (e.g. if maven is used, then mvn clean install should complete successfully)

● The API should be able to deal with time discrepancy, which means, at any point of time, we could receive a transaction which have a timestamp of the past


## Solution

Since the transaction history is not provided for by this task, it all comes down to having information about statistics
 for the last 60 seconds, and storing the transactions themselves is not required. The implementation is based
  on the fact that, in fact, the lifetime of a transaction is limited to 60 seconds, since we want to fill up the statistics
   for the last 60 seconds.

To store statistics, ConcurrentHashMap is used, where the key is the time stamp of the transaction / transactions
 (there can be many of them at the same moment) over a period of time, and the value is statistics for the last 60 seconds,
  counting from this moment. The result is a kind of floating window relative to time. This makes it possible as a result
   to obtain statistics for the last 60 seconds simply by obtaining a value for a key equal to the current moment in time,
    which is not a blocking operation. The .compute(...) method provides atomicity when creating / updating a statistics object
     for a certain point in time. Required memory and time limits are not violated.

The scheduler is used to clear old statistics from the storage. This keeps the storage size within normal limits.


## Build and run

To compile, test & package project and install .jar in local mvn repository: 

    mvn clean install

To run app: 

    mvn spring-boot:run
    
To run app using .jar: 

    java -jar target/transactions-x.x.x-SNAPSHOT.jar
    
Swagger UI url: http://localhost:8080/swagger-ui/